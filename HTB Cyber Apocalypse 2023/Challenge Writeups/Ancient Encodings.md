## Challenge Name: Ancient Encodings
Category: Crypto

Challenge Description: 
Your initialization sequence requires loading various programs to gain the necessary knowledge and skills for your journey. Your first task is to learn the ancient encodings used by the aliens in their communication.

Artifact Files:
* Encoded string = 0x53465243657a467558336b7764584a66616a4231636d347a655639354d48566664326b786246397a5a544e66644767784e56396c626d4d775a4446755a334e665a58597a636e6c33614756794d33303d
* source.py

## Approach

#### For any problem that includes an encoded string, I will always put is into CyberChef first. Seeing the "0x" at the beginning tells me that this is currently in hexadecimal format. I use the "From Hex" feature in CyberChef and get the following output:

* SFRCezFuX3kwdXJfajB1cm4zeV95MHVfd2kxbF9zZTNfdGgxNV9lbmMwZDFuZ3NfZXYzcnl3aGVyM30=

#### From this, we can deduce that it is encoded in base64 due to the "=" sign at the end. We then put "From Base64" into our CyberChef recipe and get the flag.

* HTB{1n_y0ur_j0urn3y_y0u_wi1l_se3_th15_enc0d1ngs_ev3rywher3}

  

---
[Back to home](<https://gitlab.com/crushey/ctf-writeups/-/blob/f57eed2e666fec42e1b05cf436fc6ac5e496f4de/HTB%20Cyber%20Apocalypse%202023/challenges.md>)
