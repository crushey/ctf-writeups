# HTB Cyber Apocalypse  2023

Writeups for [HTB Cyber Apocalypse 2023](<https://ctf.hackthebox.com/event/details/cyber-apocalypse-2023-the-cursed-mission-821>)

## Categories

- Crypto
   - [ ] [Ancient Encodings](<https://gitlab.com/crushey/ctf-writeups/-/blob/main/HTB%20Cyber%20Apocalypse%202023/Challenge%20Writeups/Ancient%20Encodings.md>)
   - [ ] [Small StEps](<link_to_writeup>)
   - [ ] [Perfect Synchronization](<link_to_writeup>)

- Hardware
   - [ ] [Critical Flight](<link_to_writeup>)
   - [ ] [Debug](<link_to_writeup>)

- Miscellaneous
   - [ ] [nehebkaus trap](<link_to_writeup>)
   - [ ] [Remote computation](<link_to_writeup>)
   - [ ] [Chasm's Crossing Conundrum](<link_to_writeup>)

